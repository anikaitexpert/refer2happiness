import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CommonService } from '../service/common.service';
import { ApiService } from '../service/api.service';
import { LocalService } from '../service/local.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loading = false;
  loading_message = "Loading...";
  submitted = false;
  public loginForm: FormGroup;

  constructor(public localService: LocalService,
    public apiService: ApiService,
    private navController: NavController,
    public formBuilder: FormBuilder,
    public commonService: CommonService) {

    this.loginForm = this.formBuilder.group({    
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),   
      password: new FormControl('', [Validators.required, Validators.minLength(8), Validators.maxLength(50)]),    
    });

  }

  get f() { return this.loginForm.controls; }

  async onSubmit(form) {
    this.submitted = true;

    if (this.loginForm.invalid) {
      return;
    }
    
    this.loading = true;

    this.apiService.login(form.value).subscribe((data: any) => {
      
      if (data.status == 1) {
        let user = JSON.stringify(data.detail);
        localStorage.setItem('user', user);
        this.apiService.userObj.next(user);

        this.loading_message ="Logged In"; 

        setTimeout(x => {  
          this.navController.pop();               
        }, 2000);
      }
      else {
        this.loading_message ="Invalid email & Password";
        this.commonService.presentAlert('Error', data.error);
      }
    });
  }
  ngOnInit() {}

  registration()
  {
    this.navController.pop();
    this.navController.navigateForward('/register');
  }

  goBack(e) {
    e.stopPropagation();
    this.navController.pop();
  }
}
