import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { CommonService } from '../service/common.service';
import { ApiService } from '../service/api.service';
import { LocalService } from '../service/local.service';

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.scss'],
})
export class HelpComponent implements OnInit {
  shownGroup = null;
  notifications: any;
  user: any;
  user_id: any;
  loading = false;
  constructor(public localService: LocalService,
    public apiService: ApiService,
    private navController: NavController,
    public commonService: CommonService) {
  }


  ngOnInit() {
   // this.getNotifications();
  }

  async getNotifications() {
 
  }

 
  goBack(e) {
    e.stopPropagation();
    this.navController.pop();
  }

}
