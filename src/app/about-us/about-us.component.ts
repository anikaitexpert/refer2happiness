import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss'],
})
export class AboutUsComponent implements OnInit {

  constructor(private navController: NavController) { }

  ngOnInit() { }

  goBack(e) {
    e.stopPropagation();
    this.navController.pop();
  }
}
