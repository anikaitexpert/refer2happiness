import { Component, OnInit, Renderer2 } from '@angular/core';
import { NavController,Platform  } from '@ionic/angular';
import { CommonService } from '../service/common.service';
import { ApiService } from '../service/api.service';
import { LocalService } from '../service/local.service';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
// import { SocialSharing } from '@ionic-native/social-sharing';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  slideOptsOne = {
    initialSlide: 0,
    slidesPerView: 1,
    autoplay: true
  };

  title = "Refer2happiness";
  description="Refer & Earn. Download the refer2happiness";
  url = "https://play.google.com/store/apps/details?id=com.solaceworld.refer2happiness";

  registration = true;
//private socialSharing: SocialSharing,
  constructor(
    private platform: Platform,
    private router: Router,
    public localService: LocalService,
    public iab: InAppBrowser,
    public apiService: ApiService,
    private navController: NavController,
    public commonService: CommonService,
    private renderer2: Renderer2) {
    if (this.localService.isLoggedIn()) {
      this.registration = false;
    }

  }
  ngOnInit() {
    this.renderer2.removeClass(document.body, 'inner-class');

    this.apiService.userObj.subscribe((data) => {
      if (data != null) {
        if (this.localService.isLoggedIn()) {
          this.registration = false;
        }
        else {
          this.registration = true;
        }
      }
    });


    this.platform.backButton.subscribeWithPriority(0, () => {
     if (this.router.url === '/home') {               
        navigator['app'].exitApp();
      } else {      
        this.navController.pop();
      }
    });
  }
  aboutus() {
    this.navController.navigateForward('/about-us');
  }
  help() {
    this.navController.navigateForward('/help');
  }
  register() {
    this.navController.navigateForward('/register');
  }
  productlist(id, logo) {
 //   this.navController.navigateForward('/product-list/' + id + "/" + logo);
  }
  products() {
    if (this.localService.isLoggedIn()) {
      this.navController.navigateForward('/products');
    }
    else {
      this.navController.navigateForward('/login')
    }
  }
  notifications() {
    this.navController.navigateForward('/notifications');
  }
  referral() {
    if (this.localService.isLoggedIn()) {
      this.navController.navigateForward('/referral');
    }
    else {
      this.navController.navigateForward('/login')
    }
  }
  rewards() {
    if (this.localService.isLoggedIn()) {
      this.navController.navigateForward('/rewards')
    }
    else {
      this.navController.navigateForward('/login')
    }
  }
  profile() {
    if (this.localService.isLoggedIn()) {
      this.navController.navigateForward('/profile');
    } else {
      this.navController.navigateForward('/login')
    }
  }
  contactus() {
    this.navController.navigateForward('/contactus');
  }

  OpenFacebook() {  
   const browser = this.iab.create('https://www.facebook.com/SolaceBiotechLimited/');
    browser.show()
  }

  OpenTwitter() {
    const browser = this.iab.create('https://twitter.com/solacebiotech');
    browser.show()
 
  }
  OpenLinkedin() {
    const browser = this.iab.create('https://www.linkedin.com/in/solacebiotechlimited/');
    browser.show()
   
  }
  OpenWhatsapp() {
    const browser = this.iab.create('https://web.whatsapp.com/send?phone=919355955555&text=Welcome%20to%20Solace');
    browser.show()
  }

  
}
