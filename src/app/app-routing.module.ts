import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';
import { HomeComponent } from './home/home.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { RegisterComponent } from './register/register.component';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductsComponent } from './products/products.component';
import { LoginComponent } from './login/login.component';
import { ReferralComponent } from './referral/referral.component';
import { AddReferralComponent } from './add-referral/add-referral.component';
import { ProfileComponent } from './profile/profile.component';
import { ContactusComponent } from './contactus/contactus.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { RewardsComponent } from './rewards/rewards.component';
import { HelpComponent } from './help/help.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '', component: LayoutComponent,
    children: [
      { path: 'home', component: HomeComponent },
      { path: 'about-us', component: AboutUsComponent },
      { path: 'register', component: RegisterComponent },
      { path: 'product-list/:id/:logo', component: ProductListComponent },
      { path: 'products', component: ProductsComponent },
      { path: 'login', component: LoginComponent },
      { path: 'referral', component: ReferralComponent },
      { path: 'add-referral', component: AddReferralComponent },
      { path: 'profile', component: ProfileComponent },
      { path: 'contactus', component: ContactusComponent },
      { path: 'notifications', component: NotificationsComponent },
      { path: 'rewards', component: RewardsComponent },
      { path: 'help', component: HelpComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
