import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { CommonService } from '../service/common.service';
import { ApiService } from '../service/api.service';
import { LocalService } from '../service/local.service';

@Component({
  selector: 'app-rewards',
  templateUrl: './rewards.component.html',
  styleUrls: ['./rewards.component.scss'],
})
export class RewardsComponent implements OnInit {
  shownGroup = null;
  rewards: any;
  user: any;
  user_id: any;
  loading = false;
  constructor(public localService: LocalService,
    public apiService: ApiService,
    private navController: NavController,
    public commonService: CommonService) {

      if (this.localService.isLoggedIn()) {
        this.user = this.localService.getUser();
        this.user_id = this.user.id;
      }
  }

  refresh() {
    this.getRewardsList();
  }
  ngOnInit() {
   this.getRewardsList();
  }

  async getRewardsList() {
    this.loading = true;
    this.apiService.getRewardsList(this.user_id).subscribe((data: any) => {
      this.loading = false;
      this.rewards = data.detail;
    });
  }
  getStatus(status)
  {
    if(status == 0)
    {
        return "Pending";
    }
    else if( status == 1)
    {
      return "Availed";
    }
  }
  getStatusColor(status)
  {
    if(status == 0)
    {
        return "danger";
    }
    else if( status == 1)
    {
      return "success";
    }
  }
  getStatusColor1(status)
  {
    if(status == 0)
    {
        return "red";
    }
    else if( status == 1)
    {
      return "green";
    }
  }
  toggleGroup(group) {
    if (this.isGroupShown(group)) {
      this.shownGroup = null;
    } else {
      this.shownGroup = group;
    }
  };
  isGroupShown(group) {
    return this.shownGroup === group;
  };

  goBack(e) {
    e.stopPropagation();
    this.navController.pop();
  }

}
