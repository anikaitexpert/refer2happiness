import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { CommonService } from '../service/common.service';
import { ApiService } from '../service/api.service';
import { LocalService } from '../service/local.service';


@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss'],
})
export class NotificationsComponent implements OnInit {
  shownGroup = null;
  notifications: any;
  user: any;
  user_id: any;
  loading = false;
  constructor(public localService: LocalService,
    public apiService: ApiService,
    private navController: NavController,
    public commonService: CommonService) {


  }


  ngOnInit() {
    this.getNotifications();
  }

  async getNotifications() {
    this.loading = true;
    this.apiService.getNotifications().subscribe((data: any) => {
      this.loading = false;
      this.notifications = data.detail;
    });
  }

 
  goBack(e) {
    e.stopPropagation();
    this.navController.pop();
  }

  refresh() {
    this.getNotifications();
  }
}
