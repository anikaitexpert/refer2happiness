import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { CommonService } from '../service/common.service';
import { ApiService } from '../service/api.service';
import { LocalService } from '../service/local.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss'],
})
export class ProductListComponent implements OnInit {
  shownGroup = null;
  categoryId;
  productLists: any;
  loading_message = "Loading...";
  loading = false;

  constructor(private activatedRoute: ActivatedRoute,
    public localService: LocalService,
    public apiService: ApiService,
    private navController: NavController,
    public commonService: CommonService) {
    this.categoryId = this.activatedRoute.snapshot.params.id;
   
  }

 async ngOnInit() {
    this.loading = true;
    this.apiService.getProductList(this.categoryId).subscribe((data: any) => {
      this.loading = false;
      this.productLists = data.detail;
    });
  }

  toggleGroup(group) {
    if (this.isGroupShown(group)) {
      this.shownGroup = null;
    } else {
      this.shownGroup = group;
    }
  };
  isGroupShown(group) {
    return this.shownGroup === group;
  };

  enquiry()
  {
    this.navController.navigateForward('/contactus');
  }

  goBack(e) {
    e.stopPropagation();
    this.navController.pop();
  }
}
