import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { CommonService } from '../service/common.service';
import { ApiService } from '../service/api.service';
import { LocalService } from '../service/local.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
})
export class ProductsComponent implements OnInit {
  loading_message = "Loading...";
  loading = false;
  categories:any;
  constructor(private activatedRoute: ActivatedRoute,
    public localService: LocalService,
    public apiService: ApiService,
    private navController: NavController,
    public commonService: CommonService) {
     
  }

 async ngOnInit() {
    this.loading = true;
    this.apiService.getProducts().subscribe((data: any) => {    
      this.loading = false;  
      this.categories = data.detail;

    });
  }

  goBack(e) {
    e.stopPropagation();
    this.navController.pop();
  }

  productlist(id,logo)
  {
    this.navController.navigateForward('/product-list/'+id+"/"+logo);
  }
}
