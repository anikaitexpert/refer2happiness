import { Component, OnInit, Renderer2 } from '@angular/core';
import { NavController, MenuController } from '@ionic/angular';
import { CommonService } from '../service/common.service';
import { ApiService } from '../service/api.service';
import { LocalService } from '../service/local.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
})
export class LayoutComponent implements OnInit {
  username = "demo";
  useremail = "demo@gmail.com";
  user;
  logoutbutton = false;
  constructor(public localService: LocalService,
    public apiService: ApiService,
    private navController: NavController,
    public commonService: CommonService,
    public menuCtrl: MenuController,
    private renderer2: Renderer2) {


    if (this.localService.isLoggedIn()) {
      this.user = this.localService.getUser();
      this.username = this.user.full_name;
      this.useremail = this.user.email;
      this.logoutbutton = true;
    }

  }

  aboutus() {
    this.menuCtrl.close();
    this.navController.navigateForward('/about-us');
  }
  register() {
    this.menuCtrl.close();
    this.navController.navigateForward('/register');
  }
  productlist(id, logo) {
    this.menuCtrl.close();
    this.navController.navigateForward('/product-list/' + id + "/" + logo);
  }
  // products() {
  //   this.menuCtrl.close();
  //   if (this.localService.isLoggedIn()) {
  //   this.navController.navigateForward('/products');
  // }
  // else {
  //   this.navController.navigateForward('/login')
  // }
  // }
  referral() {
    this.menuCtrl.close();
    if (this.localService.isLoggedIn()) {
      this.navController.navigateForward('/referral');
    }
    else {
      this.navController.navigateForward('/login')
    }
  }
  profile() {
    this.menuCtrl.close();
    if (this.localService.isLoggedIn()) {
      this.navController.navigateForward('/profile');
    } else {
      this.navController.navigateForward('/login')
    }
  }
  contactus() {
    this.menuCtrl.close();
    this.navController.navigateForward('/contactus');
  }

  notifications() {
    this.menuCtrl.close();
    this.navController.navigateForward('/notifications');
  }

  login() {
    this.menuCtrl.close();
    this.navController.navigateForward('/login');
  }


  rewards() {
    this.menuCtrl.close();
    if (this.localService.isLoggedIn()) {
      this.navController.navigateForward('/rewards')
    }
    else {
      this.navController.navigateForward('/login')
    }
  }
  logout() {
    this.localService.logout();
    this.menuCtrl.close();
    this.username = "demo";
    this.useremail = "demo@gmail.com";
    this.logoutbutton = false;
    this.apiService.userObj.next("blank");
  }

  ngOnInit() {
    this.apiService.userObj.subscribe((data) => {
      if (data != null) {
        console.log(data);
        if (this.localService.isLoggedIn()) {
          this.user = JSON.parse(data);
          this.username = this.user.full_name;
          this.useremail = this.user.email;
          this.logoutbutton = true;
        }
        else {
          this.logoutbutton = false;
        }
      }
    });

  }

}
