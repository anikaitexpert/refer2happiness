import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LocalService {
  url: string;
  headers: any;
  httpOptions: any;
  userObj = new BehaviorSubject(null);

  constructor(public http: HttpClient) {

    this.url = environment.url;

    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
  }


  isLoggedIn() {
    let user = localStorage.getItem('user');
    if (user) {
      return true;
    } else
      return false;
  }

  getUser() {
    let user = localStorage.getItem('user');
    return JSON.parse(user);
  }

  setUser(user) {
    localStorage.setItem('user', JSON.stringify(user));
  }


  logout(){
    // clear the localstorage
    localStorage.clear();   
  }
}
