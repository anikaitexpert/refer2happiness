import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from "src/environments/environment";
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  url: string;
  headers: any;
  httpOptions: any;
  userObj = new BehaviorSubject(null);
  registerObj = new BehaviorSubject(null);

  constructor(public http: HttpClient) {
    this.url = environment.url;
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

  }

  register(data) {
    return this.http.post(this.url + "signup", data, this.httpOptions);
  }
  updateUser(data) {
    return this.http.post(this.url + "updateUser", data, this.httpOptions);
  }
  contactSubmit(data) {
    return this.http.post(this.url + "contactSubmit?name="+data.name+"&mobile="+data.mobile+"&email="+data.email+"&data="+data.query, this.httpOptions);
  }
  login(data) {
    return this.http.post(this.url + "login", data, this.httpOptions);
  }

  getProductList(id) {
    return this.http.get(this.url + "getProductList?id=" + id, this.httpOptions);
  }

  getNotifications()
  {
    return this.http.get(this.url + "getNotifications", this.httpOptions);
  }

  getCustomerList(id) {
    return this.http.get(this.url + "getCustomerList?id=" + id, this.httpOptions);
  }

  getRewardsList(id) {
    return this.http.get(this.url + "getRewardsList?id=" + id, this.httpOptions);
  }

  getProducts() {
    return this.http.get(this.url + "getProducts", this.httpOptions);
  }
  getCategories(value) {
    return this.http.get(this.url + "getCategories?id=" + value, this.httpOptions);
  }

  addReferral(name, email, mobile, id) {
    return this.http.get(this.url + "addReferral?id=" + id + "&name=" + name + "&email=" + email + "&mobile=" + mobile);
  }
}
