import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CommonService } from '../service/common.service';
import { ApiService } from '../service/api.service';
import { LocalService } from '../service/local.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  categories: any;
  submitted = false;
  public registerForm: FormGroup;
  loading = false;
  loading_message = "Loading...";
  loading2 =false;
  emailvalid = false;

  constructor(public localService: LocalService,
    public apiService: ApiService,
    private navController: NavController,
    public formBuilder: FormBuilder,
    public commonService: CommonService) {

    this.registerForm = this.formBuilder.group({
      full_name: new FormControl('', Validators.compose([Validators.required,Validators.pattern('^[A-Za-z? ]+$')])),
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      phone: new FormControl('', Validators.compose([Validators.required,Validators.pattern('[0-9]*'),Validators.minLength(10),Validators.maxLength(10)])),
      city: new FormControl('', Validators.required),
      state: new FormControl('', Validators.required),
      password: new FormControl('', [Validators.required, Validators.minLength(8), Validators.maxLength(50)]),
    });

  }

  get f() { return this.registerForm.controls; }

  async onSubmit(form) {
    this.submitted = true;

    if (this.registerForm.invalid) {
      return;
    }

   this.loading = true;

    this.apiService.register(form.value).subscribe((data: any) => {
      if (data.status == 1) {
        this.loading_message = "Registration Successfully.";
        let user = JSON.stringify(data.detail);
        localStorage.setItem('user', user);
        this.apiService.userObj.next(user);
        this.commonService.presentAlert('Success',"Registration Successfully.");
        setTimeout(x => {
          this.navController.pop();
        }, 2000);
      }
      else {
        this.loading = false;
        this.commonService.presentAlert('Error', data.error);
      }
    });
  }

  checkEmail(ev)
  {
    console.log(ev.target.value);
    if(ev.target.value != "")
    {
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
      if (reg.test(ev.target.value) == false)
      {
         this.emailvalid = true;
      }
      else
      {
        this.emailvalid = false;
      }
    }else
    {
      this.emailvalid = false;
    }
  }
  async onchanged(value) {
    
    if (value == 1) {
      this.loading2 = true;
      
      this.apiService.getCategories(value).subscribe((data: any) => {
        this.categories = data.detail;
        this.loading2 =false;
      });
    }
    else if (value == 2) {
   
      this.loading2 = true;
      this.apiService.getCategories(value).subscribe((data: any) => {
        this.categories = data.detail;
        this.loading2 =false;
      });
    }
  }

  ngOnInit() {

  }

  goBack(e) {
    e.stopPropagation();
    this.navController.pop();
  }
}
