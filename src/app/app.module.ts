import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './home/home.component';
import { LayoutComponent } from './layout/layout.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { RegisterComponent } from './register/register.component';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductsComponent } from './products/products.component';
import { LoginComponent } from './login/login.component';
import { ReferralComponent } from './referral/referral.component';
import { AddReferralComponent } from './add-referral/add-referral.component';
import { ProfileComponent } from './profile/profile.component';
import { ContactusComponent } from './contactus/contactus.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { RewardsComponent } from './rewards/rewards.component';
import { HelpComponent } from './help/help.component';

import { ApiService } from './service/api.service';
import { LocalService } from './service/local.service';
import { CommonService } from './service/common.service';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LayoutComponent,
    AboutUsComponent,
    RegisterComponent,
    ProductListComponent,
    ProductsComponent,
    LoginComponent,
    ReferralComponent,
    AddReferralComponent,
    ProfileComponent,
    ContactusComponent,
    NotificationsComponent,
    RewardsComponent,
    HelpComponent
  ],
  entryComponents: [],
  imports: [BrowserModule, 
    IonicModule.forRoot(), 
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ApiService, 
    LocalService,
    CommonService,
    InAppBrowser
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
