import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CommonService } from '../service/common.service';
import { ApiService } from '../service/api.service';
import { LocalService } from '../service/local.service';

@Component({
  selector: 'app-add-referral',
  templateUrl: './add-referral.component.html',
  styleUrls: ['./add-referral.component.scss'],
})
export class AddReferralComponent implements OnInit {
  submitted = false;
  user: any;
  userId = 0;
  public addReferralForm: FormGroup;
  loading = false;
  loading_message = "Loading...";
  emailvalid = false;
  mydata = {'full_name':"vikas","email":"saini391@gmail.com","mobile":"9837483749"};

  constructor(public localService: LocalService,
    public apiService: ApiService,
    private navController: NavController,
    public formBuilder: FormBuilder,
    public commonService: CommonService) {

    if (this.localService.isLoggedIn()) {
      this.user = this.localService.getUser();
      this.userId = this.user.id;
    }

    this.addReferralForm = this.formBuilder.group({
      full_name: new FormControl('',  Validators.compose([Validators.required,Validators.pattern('^[A-Za-z? ]+$')])),
      email: new FormControl('', Validators.required),
      mobile: new FormControl('', Validators.compose([Validators.required,Validators.pattern('[0-9]*'),Validators.minLength(10),Validators.maxLength(10)])),
    });


  }
  get f() { return this.addReferralForm.controls; }

  async onSubmit(form) {
    this.submitted = true;

    if (this.addReferralForm.invalid) {
      return;
    }
   
    this.loading = true;

    this.apiService.addReferral(form.value.full_name,form.value.email,form.value.mobile, this.userId).subscribe((data: any) => {
     
      if (data.status == 1) {
        this.loading_message = "Inserted";   
        this.commonService.presentAlert('Info',"Inserted");   
        setTimeout(x => {
          this.navController.pop();
        }, 2000);
      }
      else {
        this.commonService.presentAlert('Error', data.error);
      }
    });
  }

  checkEmail(ev)
  {
    console.log(ev.target.value);
    if(ev.target.value != "")
    {
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
      if (reg.test(ev.target.value) == false)
      {
         this.emailvalid = true;
      }
      else
      {
        this.emailvalid = false;
      }
    }else
    {
      this.emailvalid = false;
    }
  }

  ngOnInit() { }

  goBack(e) {
    e.stopPropagation();
    this.navController.pop();
  }
}
